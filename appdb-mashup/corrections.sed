# Apdb Game Title Corrections
# These change titles from how they appear in the appdb to how they appear in gamerankings.com
s/Baldur's Gate II$/Baldur's Gate II: Shadows of Amn/
s/America's Army: Special Forces/America's Army/
s/Sid Meier's Civilization/Civilization/
s/Everquest 2/EverQuest II/
s/Evidence, The last Ritual/Evidence: The Last Ritual/
s/F.E.A.R.: First Encounter Assault Recon/F.E.A.R./
s/Final Fantasy XI Online/Final Fantasy XI/
s/Lara Croft Tomb Raider/Tomb Raider/
s/Medieval 2/Medieval II/
s/Midnight Club 2/Midnight Club II/
s/Neverwinter Nights II$/Neverwinter Nights 2/
s/Secret Of Monkey Island Special Edition/The Secret of Monkey Island: Special Edition/
s/Splinter Cell Chaos Theory/Tom Clancy's Splinter Cell Chaos Theory/ 
s/Tom Clancy's Splinter Cell: /Tom Clancy's Splinter Cell / 
s/Tomb Raider Anniversary/Tomb Raider: Anniversary/
s/The Lord of the Rings: The Battle for Middle-Earth/The Lord of the Rings, The Battle for Middle-earth/
s/Warlords: Battlecry/Warlords Battlecry/
s/X-Men: Legends II - Rise of Apocalypse/X-Men Legends II: Rise of Apocalypse/
s/Zuma's Revenge/Zuma's Revenge!/
